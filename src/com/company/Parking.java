package com.company;
import java.util.ArrayList;
import java.util.Random;

public class Parking {
    Random random = new Random();

    public ArrayList<Auto> createParkingPlace(int parkingPlaceCars, int parkingPlaceTrucks) {

        ArrayList<Auto> parkingPlaceList = new ArrayList<>(parkingPlaceCars + parkingPlaceTrucks);

        for (int i = 0; i < parkingPlaceCars + parkingPlaceTrucks; i++) {
            parkingPlaceList.add(i, null);
        }

        return parkingPlaceList;
    }


    public ArrayList<Truck> generateTrucks(int numberOfTrucks) {

        ArrayList<Truck> trucks = new ArrayList<>(numberOfTrucks);
        for (int i = 0; i < numberOfTrucks; i++) {
            trucks.add(i, null);
        }
        for (int i = 0; i < numberOfTrucks; i++) {
            StringBuilder number = new StringBuilder();
            for (int j = 0; j < 4; j++) {
                int oneNumber = random.nextInt(10);
                number.append(oneNumber);
            }
            trucks.set(i, new Truck(number.toString(), random.nextInt(10) + 1));
        }

        return trucks;
    }

    public ArrayList<Car> generateCars(int numberOfCars) {

        ArrayList<Car> cars = new ArrayList<>(numberOfCars);
        for (int i = 0; i < numberOfCars; i++) {
            cars.add(i, null);
        }
        for (int i = 0; i < numberOfCars; i++) {
            StringBuilder number = new StringBuilder();
            for (int j = 0; j < 4; j++) {
                int oneNumber = random.nextInt(10);
                number.append(oneNumber);
            }
            cars.set(i, new Car(number.toString(), random.nextInt(10) + 1));
        }


        return cars;
    }


    public int generateNumberOfCars(int parkingPlaces) {
        return random.nextInt(parkingPlaces / 3 + 1);
    }

    public ArrayList<Auto> fillParkingPlace(ArrayList<Auto> parkingPlaceList, ArrayList<Truck> trucks, ArrayList<Car> cars,
                                            int parkingPlaceTrucks, int parkingPlaceAll) {
        int leftCars = 0;
        int leftTrucks = 0;

        for (int i = 0; i < parkingPlaceAll; i++) {
            if (parkingPlaceList.get(i) != null) {

                if (parkingPlaceList.get(i).time != 0) {
                    parkingPlaceList.get(i).time -= 1;
                }
                if (parkingPlaceList.get(i).time == 0) {
                    parkingPlaceList.set(i, null);
                }
            }
        }

        for (int i = 0; i < trucks.size(); i++) {
            int fitTruck = -1;
            for (int j = 0; j < parkingPlaceTrucks; j++) {

                if (parkingPlaceList.get(j) == null) {

                    parkingPlaceList.set(j, new Auto(trucks.get(i).number, trucks.get(i).time));
                    fitTruck = 0;
                    break;
                }
            }

            if (fitTruck != 0) {
                leftTrucks++;
            }
        }

        for (int i = 0; i < cars.size(); i++) {
            int fitCar = -1;
            for (int j = parkingPlaceTrucks; j < parkingPlaceAll; j++) {

                if (parkingPlaceList.get(j) == null) {

                    parkingPlaceList.set(j, new Auto(cars.get(i).number, cars.get(i).time));
                    fitCar = 0;
                    break;
                }
            }

            if (fitCar != 0) {
                leftCars++;
            }
        }

        int leftTrucksFromTruckParking = 0;

        for (int i = trucks.size() - leftTrucks; i < trucks.size(); i++) {
            int fitTruck = -1;
            for (int j = parkingPlaceTrucks; j < parkingPlaceAll - 1; j++) {
                if (parkingPlaceList.get(j) == null && parkingPlaceList.get(j + 1) == null) {
                    fitTruck = 0;
                    parkingPlaceList.set(j, new Auto(trucks.get(i).number, trucks.get(i).time));
                    parkingPlaceList.set(j + 1, new Auto(trucks.get(i).number, trucks.get(i).time));

                    j++;
                    break;
                }

            }
            if (fitTruck != 0) {
                leftTrucksFromTruckParking++;
            }
        }

        leftTrucks = leftTrucksFromTruckParking;

        if (leftCars != 0 && leftTrucks != 0) {
            System.out.println("На парковку не уместилось " + leftCars + " машин и " + leftTrucks + " грузовиков.");

        } else if (leftTrucks != 0) {
            System.out.println("На парковку не уместилось " + leftTrucks + " грузовиков.");

        } else if (leftCars != 0) {

            System.out.println("\n" + "На парковку не уместилось " + leftCars + " машин.");

        }
        if (leftCars != 0 || leftTrucks != 0) {
            freeParkingPlaces(parkingPlaceList, parkingPlaceTrucks, parkingPlaceAll);
        }

        return parkingPlaceList;
    }

    public ArrayList<Auto> cleanParking(ArrayList<Auto> parkingPlaceList, int parkingPlaceAll) {

        for (int i = 0; i < parkingPlaceAll; i++) {
            parkingPlaceList.set(i, null);
        }
        return parkingPlaceList;
    }


    public int getFreePlaces(ArrayList<Auto> parkingPlaceList, int parkingPlaceAll) {
        int freePlace = 0;
        for (int i = 0; i < parkingPlaceAll; i++) {
            if (parkingPlaceList.get(i) == null) {
                freePlace++;
            }
        }
        return freePlace;
    }


    public void arrivedCars(ArrayList<Car> cars) {
        if (cars.size() == 0) {
            System.out.println("На парковку не приехало машин.");
        }
        for (int i = 0; i < cars.size(); i++) {
            System.out.println("На парковку приехала машина с номером " + cars.get(i).number
                    + " и временем " + cars.get(i).time + ".");
        }
        System.out.println();

    }


    public void arrivedTrucks(ArrayList<Truck> trucks) {

        if (trucks.size() == 0) {
            System.out.println("На парковку не приехало грузовиков.");
        }
        for (int i = 0; i < trucks.size(); i++) {
            System.out.println("На парковку приехал грузовик с номером " + trucks.get(i).number
                    + " и временем " + trucks.get(i).time + ".");
        }
    }


    public void freeParkingPlaces(ArrayList<Auto> parkingPlaceList, int parkingPlaceTrucks, int parkingPlaceAll) {
        int minCarTime = 11;
        int minTruckTime = 11;


        for (int i = 0; i < parkingPlaceTrucks; i++) {
            if (parkingPlaceList.get(i) != null) {

                if (parkingPlaceList.get(i).time < minTruckTime) {
                    minTruckTime = parkingPlaceList.get(i).time;
                }
            }else{
                minTruckTime = 0;
                break;
            }
        }

        for (int i = parkingPlaceTrucks; i < parkingPlaceAll; i++) {
            if (parkingPlaceList.get(i) != null) {

                if (parkingPlaceList.get(i).time < minCarTime) {
                    minCarTime = parkingPlaceList.get(i).time;
                }
            }else{
                minCarTime = 0;
                break;
            }
        }

        if (minTruckTime !=0 && minCarTime != 0) {
            System.out.println("\n" + "Ближайшее место на парковке машин освободится через " + (minCarTime) + " ход." + "\n" +
                    "Ближайшее место на парковке грузовиков освободится через " + (minTruckTime) + " ход.");

        } else if (minTruckTime != 0) {

            System.out.println("\n" + "Ближайшее место на парковке грузовиков освободится через " + (minTruckTime) + " ход." + "\n" +
                    "На парковке машин есть свободные места.");

        } else if (minCarTime != 0) {

            System.out.println("\n" + "На парковке грузовиков есть свободные места." + "\n" +
                    "Ближайшее место на парковке машин освободится через " + (minCarTime) + " ход." + "\n");
        } else {
            System.out.println("На парковке есть свободные места.");
        }

    }


    public void showParkingPlaces(ArrayList<Auto> parkingPlaceList, int parkingPlaceTrucks, int parkingPlaceAll) {

        for (int i = 0; i < parkingPlaceTrucks; i++) {
            if (parkingPlaceList.get(i) != null) {

                System.out.println("На " + (i + 1) + " месте парковки стоит грузовик с номером "
                        + parkingPlaceList.get(i).number + " и оставшимся временем "
                        + parkingPlaceList.get(i).time);

            } else {
                System.out.println("На " + (i + 1) + " месте нет грузовика.");
            }
        }


        for (int i = parkingPlaceTrucks; i < parkingPlaceAll; i++) {
            if (parkingPlaceList.get(i) != null) {
                if ((i != parkingPlaceAll - 1) && parkingPlaceList.get(i + 1) != null) {

                    if ((parkingPlaceList.get(i).number.equals(parkingPlaceList.get(i + 1).number))) {
                        System.out.println("На " + (i + 1) + " и " + (i + 2) + " месте парковки стоит грузовик с номером "
                                + parkingPlaceList.get(i).number + " и оставшимся временем "
                                + parkingPlaceList.get(i).time);
                        i++;
                        continue;

                    }

                }
                System.out.println("На " + (i + 1) + " месте парковки стоит машина с номером "
                        + parkingPlaceList.get(i).number + " и оставшимся временем "
                        + parkingPlaceList.get(i).time);


            } else {
                System.out.println("На " + (i + 1) + " месте нет машины.");
            }
        }
        System.out.println();
    }
}
