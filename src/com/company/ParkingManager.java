package com.company;

import java.util.ArrayList;
import java.util.Scanner;


public class ParkingManager {

    public static void main(String[] args) {
        outerLoop:
        while (true) {
            Scanner scanner = new Scanner(System.in);
            int parkingPlaceTrucks = 0;
            int parkingPlaceCars = 0;

            while (parkingPlaceTrucks < 1 || parkingPlaceCars < 3) {
                System.out.println("Введите количество парковочных мест для грузовиков.");
                parkingPlaceTrucks = scanner.nextInt();

                System.out.println("Введите количество парковочных мест для машин.");
                parkingPlaceCars = scanner.nextInt();

                if (parkingPlaceTrucks < 1) {
                    System.out.println("Минимальное количество мест для грузовых машин - 1." + "\n");

                }
                if (parkingPlaceCars < 3) {
                    System.out.println("Минимальное количество мест для обычных машин - 3." + "\n" );

                }
            }

            info();
            Parking parking = new Parking();
            int parkingPlaceAll = parkingPlaceCars + parkingPlaceTrucks;

            ArrayList<Auto> parkingPlace = parking.createParkingPlace(parkingPlaceCars, parkingPlaceTrucks);
            ArrayList<Truck> trucks = new ArrayList<>(0);
            ArrayList<Car> cars = new ArrayList<>(0);

            while (true) {
                switch (scanner.next()) {
                    case ("1"):
                        trucks = parking.generateTrucks(parking.generateNumberOfCars(parkingPlaceAll));
                        cars = parking.generateCars(parking.generateNumberOfCars(parkingPlaceAll));

                        parkingPlace = parking.fillParkingPlace(parkingPlace, trucks, cars, parkingPlaceTrucks, parkingPlaceAll);

                        break;
                    case ("2"):
                        parkingPlace = parking.cleanParking(parkingPlace, parkingPlaceAll);
                        System.out.println("Парковочные места очищены");
                        break;
                    case ("3"):
                        System.out.println("На паркове осталось " + parking.getFreePlaces(parkingPlace, parkingPlaceAll) + " свободных мест.");
                        break;
                    case ("4"):
                        System.out.println("На паркове занято " + (parkingPlaceCars + parkingPlaceTrucks - parking.getFreePlaces(parkingPlace, parkingPlaceAll)) + " места.");
                        break;
                    case ("5"):
                        parking.showParkingPlaces(parkingPlace, parkingPlaceTrucks, parkingPlaceAll);
                        break;
                    case ("6"):
                        parking.arrivedTrucks(trucks);
                        parking.arrivedCars(cars);
                        break;
                    case ("7"):
                        parking.freeParkingPlaces(parkingPlace, parkingPlaceTrucks, parkingPlaceAll);
                        break;
                    case ("8"):
                        info();
                        break;
                    case ("9"):
                        break outerLoop;
                    default:
                        System.out.println("Такой команды не существует." + "\n");
                        info();
                }
            }



        }
    }

    public static void info(){
        System.out.println("Доступные команды: 1 - сделать ход," + "\n" +
                "2 - очистить все парковочные места,"  + "\n" +
                "3 - узнать кол-во свободных мест," + "\n" +
                "4 - узнать кол-во занятых мест,"+ "\n" +
                "5 - показать, какая машина стоит на каком парковочном месте,"+ "\n" +
                "6 - показать, какие машины приехали в текущий ход, " + "\n" +
                "7 - показать, через сколько ходов освободится ближайшее место на парковке, " + "\n" +
                "8 - получить информацию по командам,"+ "\n" +
                "9 - закрыть парковку.");
    }
}
